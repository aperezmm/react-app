import React, { useState, useEffect } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

import { WelcomeComponent } from './components/Welcome';

import { ProductsComponent } from './components/ProductsComponent';

function App() {
  const [count, setCount] = useState(1000);
  const [ products, setProducts] = useState([]);
  const [ username, setUsername] = useState("");
  const [ isUserLogged, setIsUserLogged] = useState(false);
  
  const calculateCountValue = () => {
    if(count > 1500) return;

    setCount(2000);
  }

  useEffect(() => {
    console.log("Cambiando is log user");
    setIsUserLogged(true);
  },[])

  useEffect(() => {
    if(count < 1500) {
      console.log("El count menor a 1500");
    }
    
    if(count > 1500){
      console.log("El count mayor a 1500");
    }
  },[count])

  return (
    <div className="App">
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 50)}>
          Soy un contador {count}
        </button>
        <hr></hr>

        {(count > 10000) && <WelcomeComponent usuarioActivo={true} name="Juan"/>}
         
         <br></br>
         <hr></hr>
         <ProductsComponent/>
      </div>
    </div>
  )
}

export default App
